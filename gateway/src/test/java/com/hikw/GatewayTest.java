package com.hikw;

import java.time.ZonedDateTime;

/**
 * @Description:
 * @ClassNmae: GatewayTest
 * @Author: HeYue
 * @DateTime: 2021-07-16 23:16
 **/
public class GatewayTest {

    public static void main(String[] args) {
        //获取时区时间
        ZonedDateTime now = ZonedDateTime.now();
        System.out.println("now = " + now);
    }

}
