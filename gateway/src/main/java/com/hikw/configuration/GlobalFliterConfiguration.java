//package com.hikw.configuration;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.Ordered;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
///**
// * @Description: 全局Filter配置
// * @ClassNmae: GlobalFliterConfiguration
// * @Author: HeYue
// * @DateTime: 2021-07-17 00:59
// **/
//@Configuration
//@Slf4j
//public class GlobalFliterConfiguration implements GlobalFilter, Ordered {
//
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        log.info("进入到自定义filter...");
//        if (exchange.getRequest().getQueryParams().get("id") != null) {
//            log.info("用户身份信息合法，已放行...");
//            return chain.filter(exchange);
//        }
//        log.info("用户信息不合法，拒绝访问...");
//        return exchange.getResponse().setComplete();
//    }
//
//    //数字越小filter最先执行  -1最先执行
//    @Override
//    public int getOrder() {
//        log.info("order已执行...");
//        return -1;
//    }
//}
