//package com.hikw.configuration;
//
//import org.springframework.cloud.gateway.route.RouteLocator;
//import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Description: 路由配置
// * @ClassNmae: RouteConfiguration
// * @Author: HeYue
// * @DateTime: 2021-07-16 15:49
// **/
//@Configuration
//public class RouteConfiguration {
//
//    @Bean
//    public RouteLocator routeLocator(RouteLocatorBuilder routeLocatorBuilder) {
//        return routeLocatorBuilder.routes()
//                .route("user_route", r -> r.path("/user/**").uri("http://127.0.0.1:9001"))
//                .route("product_route", r -> r.path("/product/**").uri("http://127.0.0.1:9002"))
//                .build();
//    }
//
//}
