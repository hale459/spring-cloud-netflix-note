package com.hikw.controller;

import com.hikw.entity.Product;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 商品接口
 * @ClassNmae: ProductController
 * @Author: HeYue
 * @DateTime: 2021-07-14 02:14
 **/
@RestController
@Slf4j
public class ProductController {

    @Value("${server.port}")
    private int port;

    @GetMapping("/product/getinfo")
    public String getProductInfo() {
        log.info("显示商品信息...");
        return "显示商品信息...当前端口:" + port;
    }

    @GetMapping("/product/getAll")
    public Map<String, Object> getAll() {
        Map<String, Object> map = new HashMap<>();
        log.info("查询所有商品信息");
        map.put("status", true);
        map.put("message", "查询所有商品信息成功，当前端口：" + port);
        return map;
    }

    @GetMapping("/product/getone")
    public Map<String, Object> getOne(int ProductId) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", "调用成功");
        map.put("value", ProductId);
        map.put("port", port);
        map.put("status", true);
        return map;
    }

    @PostMapping("/product/save")
    //@RequestBody 将json格式的字符串转换成对象
    public Map<String, Object> save(@RequestBody Product product) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", "调用成功");
        map.put("value", product);
        map.put("port", port);
        map.put("status", true);
        return map;
    }

    /**
     * @Description: 测试服务熔断
     * @Author: HeYue
     * @DateTime: 2021/7/15 17:12
     **/
    @GetMapping("/product/break")
    //熔断注解  指定熔断方法  当发生熔断时直接执行该方法
//    @HystrixCommand(fallbackMethod = "testBreakFallBack")//默认熔断方法
    @HystrixCommand(defaultFallback = "DefaultFallBack")
    public String testBreak(@RequestParam("id") Integer id) {
        if (id < 0) {
            throw new RuntimeException("非法参数，ID不能小于0！");
        }
        return "访问成功，当前ID为：" + id;
    }

    /**
     * @Description: testBreak触发熔断的方法
     * @Author: HeYue
     * @DateTime: 2021/7/15 17:21
     **/
    public String testBreakFallBack(@RequestParam("id") Integer id) {
        return "当前参数ID不合法，触发熔断！";
    }

    /**
     * @Description: 默认触发熔断的方法
     * @Author: HeYue
     * @DateTime: 2021/7/15 17:21
     **/
    public String DefaultFallBack() {
        return "服务不可用，触发熔断！";
    }
}
