package com.hikw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @ClassNmae: OrderController
 * @Author: HeYue
 * @DateTime: 2021-07-18 16:04
 **/
@RestController
@RequestMapping("order")
@RefreshScope
public class OrderController {

    @Value("${server.port}")
    private int port;

    @GetMapping("/getPort")
    public String getPort() {
        return "当前端口为：" + port;
    }
}
