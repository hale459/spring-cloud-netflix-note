package com.hikw.configuration;

import com.netflix.loadbalancer.*;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Description: Bena配置
 * @ClassNmae: BenaConfiguration
 * @Author: HeYue
 * @DateTime: 2021-07-14 01:40
 **/
@Configuration
public class BenaConfiguration {

    @Bean
    //该注解用于实现负载均衡
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    //服务策论配置
    @Bean
    public IRule iRule() {
        return new RandomRule();//随机策论
//        return new RoundRobinRule();//轮询策略(默认)
//        return new AvailabilityFilteringRule();//首先过滤掉有问题的服务，对剩下的服务进行轮询
//        return new RetryRule();//首先通过轮询获取服务，若获取失败则会在指定时间内进行重试
    }


}
