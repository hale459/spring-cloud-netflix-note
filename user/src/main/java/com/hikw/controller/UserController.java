package com.hikw.controller;

import com.hikw.clients.ProductClient;
import com.hikw.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description: 用户接口
 * @ClassNmae: UserController
 * @Author: HeYue
 * @DateTime: 2021-07-14 02:26
 **/
@RestController
@Slf4j
public class UserController {

    @Autowired
    private ProductClient productClient;

    //默认执行负载均衡策略
    @GetMapping("/user/feign/test")
    public String feignTest() {
        log.info("进入feign测试方法...");
        return "调用信息返回：" + productClient.getAllProduct();
    }

    @GetMapping("/user/feign/testget")
    public Map<String, Object> feignGet(int id) {
        log.info("进入feign--GET测试方法...");
        return productClient.getOne(id);
    }

    @PostMapping("/user/feign/testpost")
    public Map<String, Object> testPost(Product product) {
        log.info("进入feign--POST测试方法...");
        return productClient.save(product);
    }

}
