package com.hikw.entity;

import lombok.Data;

/**
 * @Description:
 * @ClassNmae: Product
 * @Author: HeYue
 * @DateTime: 2021-07-14 18:07
 **/
@Data
public class Product {

    private Integer id;
    private String name;
    private Double price;
    private String info;

}
