package com.hikw.clients;

import com.hikw.entity.Product;
import com.hikw.fallback.ProductFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Description: 商品客户端
 * @ClassNmae: ProductClient
 * @Author: HeYue
 * @DateTime: 2021-07-14 16:50
 **/
//表示当前接口是一个Feign组件，其中对应的是服务ID
@FeignClient(value = "products", fallback = ProductFallBack.class)
public interface ProductClient {

    @GetMapping("/product/getAll")
//对应product的路径即可
    String getAllProduct();//方法名随意写(可以和原方法名一致)

    //GET参数传递
    @GetMapping("/product/getone")
    Map<String, Object> getOne(@RequestParam("ProductId") int ProductId);//@RequestParam("对应原方法中的参数名"）

    //POST参数传递
    @PostMapping("/product/save")
    Map<String, Object> save(@RequestBody Product product);
}
