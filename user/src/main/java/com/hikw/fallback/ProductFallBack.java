package com.hikw.fallback;

import com.hikw.clients.ProductClient;
import com.hikw.entity.Product;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 商品FallBack
 * @ClassNmae: ProductFallBack
 * @Author: HeYue
 * @DateTime: 2021-07-15 20:55
 **/
@Component//在Spring工厂中创建实例对象
public class ProductFallBack implements ProductClient {

    @Override
    public String getAllProduct() {
        return "getAllProduct服务被降级...";
    }

    @Override
    public Map<String, Object> getOne(int ProductId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("status", true);
        map.put("values", "getOne服务被降级...");
        return map;
    }

    @Override
    public Map<String, Object> save(Product product) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("status", true);
        map.put("values", "save服务被降级...");
        return map;
    }
}
